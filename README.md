# Case Study 2 - Bank Real-Time Fraud detection

```
- Check if a given customer in a given bank transaction is already alarmed in 'alarmed_customers' data
- Check if the used credit card is mentioned as lost in lost 'lost_cards' data
- Number of transaction per customer should be les than 10 transactions per minute
- City/Location of transaction should not change more than 2 times within interval of 1 min 
```
