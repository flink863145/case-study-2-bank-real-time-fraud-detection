package src.main.java.org.example;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.functions.co.KeyedBroadcastProcessFunction;
import org.apache.flink.util.Collector;

import java.util.Map;

import static src.main.java.org.example.Bank.alarmedCustStateDescriptor;

public  class AlarmedCustCheck extends KeyedBroadcastProcessFunction< String, Tuple2< String, String >, AlarmedCustomer,
        Tuple2 < String, String >> {

    public void processElement(Tuple2 < String, String > value, ReadOnlyContext ctx, Collector< Tuple2 < String, String >> out) throws Exception {
        for (Map.Entry < String, AlarmedCustomer > custEntry: ctx.getBroadcastState(alarmedCustStateDescriptor).immutableEntries()) {
            final String alarmedCustId = custEntry.getKey();
            final AlarmedCustomer cust = custEntry.getValue();

            // get customer_id of current transaction
            final String tId = value.f1.split(",")[3];
            if (tId.equals(alarmedCustId)) {
                out.collect(new Tuple2 < String, String > ("____ALARM___", "Transaction: " + value + " is by an ALARMED customer"));
            }
        }
    }

    public void processBroadcastElement(AlarmedCustomer cust, Context ctx, Collector < Tuple2 < String, String >> out) throws Exception {
        ctx.getBroadcastState(alarmedCustStateDescriptor).put(cust.id, cust);
    }
}
