package src.main.java.org.example;

// defined according to the data schema of alarmed_cust.txt
public class AlarmedCustomer {

    public final String id;
    public final String account;

    public AlarmedCustomer(){
	id = "";
	account = "";
    }

    public AlarmedCustomer(String data){
	String[] words = data.split(",");
	id = words[0];
	account = words[1];
    }

}
