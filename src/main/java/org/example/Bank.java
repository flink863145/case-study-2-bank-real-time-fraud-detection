package src.main.java.org.example;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.serialization.SimpleStringEncoder;
import org.apache.flink.api.common.state.MapStateDescriptor;
import org.apache.flink.api.common.typeinfo.BasicTypeInfo;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.core.fs.Path;
import org.apache.flink.streaming.api.datastream.BroadcastStream;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.filesystem.StreamingFileSink;
import org.apache.flink.streaming.api.functions.sink.filesystem.rollingpolicies.DefaultRollingPolicy;
import org.apache.flink.streaming.api.windowing.assigners.TumblingProcessingTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;


public class Bank {

  // for optimisation and inorder to reduce the latency, the 2 small files that define the criteria of alarmed customer and lost cards could be broadcasted on the main data
  //file of bank transactions which has a bigger size

  //since we are broadcasting, we have use descriptor states
  public static final MapStateDescriptor < String, AlarmedCustomer > alarmedCustStateDescriptor =
    new MapStateDescriptor < String, AlarmedCustomer > ("alarmed_customers", BasicTypeInfo.STRING_TYPE_INFO, Types.POJO(AlarmedCustomer.class));

  public static final MapStateDescriptor < String, LostCard> lostCardStateDescriptor =
    new MapStateDescriptor < String, LostCard> ("lost_cards", BasicTypeInfo.STRING_TYPE_INFO, Types.POJO(LostCard.class));


  public static void main(String[] args) throws Exception {
    final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

    DataStream < AlarmedCustomer > alarmedCustomers = env.readTextFile("/home/myvm/Flink/Case Study 2 - Bank Real-Time Fraud detection/Case Study 2 - Bank Real-Time Fraud detection/src/main/resources/alarmed_cust.txt")
      .map(new MapFunction < String, AlarmedCustomer > () {
        public AlarmedCustomer map(String value) {
          return new AlarmedCustomer(value);
        }
      });
    // broadcast alarmed customer data
    BroadcastStream < AlarmedCustomer > alarmedCustBroadcast = alarmedCustomers.broadcast(alarmedCustStateDescriptor);

    DataStream <LostCard> lostCards = env.readTextFile("/home/myvm/Flink/Case Study 2 - Bank Real-Time Fraud detection/Case Study 2 - Bank Real-Time Fraud detection/src/main/resources/lost_cards.txt")
      .map(new MapFunction < String, LostCard> () {
        public LostCard map(String value) {
          return new LostCard(value);
        }
      });

    // broadcast lost card data
    BroadcastStream <LostCard> lostCardBroadcast = lostCards.broadcast(lostCardStateDescriptor);

    // transaction data keyed by customer_id
    DataStream < Tuple2 < String, String >> data = env.socketTextStream("localhost", 9090)
      .map(new MapFunction < String, Tuple2 < String, String >> () {
        public Tuple2 < String, String > map(String value) {
          String[] words = value.split(",");

          return new Tuple2 < String, String > (words[3], value); //{(id_347hfx) (HFXR347924,2018-06-14 23:32:23,Chandigarh,id_347hfx,hf98678167,123302773033,774
        }
      });

    // (1) Check against alarmed customers
    DataStream < Tuple2 < String, String >> alarmedCustTransactions = data
      .keyBy(t -> t.f0)
      .connect(alarmedCustBroadcast)
      .process(new AlarmedCustCheck());

    // (2) Check against lost cards
    DataStream < Tuple2 < String, String >> lostCardTransactions = data
      .keyBy(t -> t.f0)
      .connect(lostCardBroadcast)
      .process(new LostCardCheck());


    // (3) More than 10 transactions check
    DataStream < Tuple2 < String, String >> excessiveTransactions = data
      .map(new MapFunction < Tuple2 < String, String > , Tuple3 < String, String, Integer >> () {
        public Tuple3 < String, String, Integer > map(Tuple2 < String, String > value) {
          return new Tuple3 < String, String, Integer > (value.f0, value.f1, 1);
        }
      })
      .keyBy(t -> t.f0)
      .window(TumblingProcessingTimeWindows.of(Time.seconds(10)))
      .sum(2)
      .flatMap(new FilterAndMapMoreThan10());


    // (4) city change check
    DataStream < Tuple2 < String, String >> freqCityChangeTransactions = data
      .keyBy(t -> t.f0)
      .window(TumblingProcessingTimeWindows.of(Time.seconds(10)))
      .process(new Citychange());

    // regrouping all 4 checks in 1 union to execute!
    DataStream < Tuple2 < String, String >> AllFlaggedTxn =
      alarmedCustTransactions.union(lostCardTransactions, excessiveTransactions, freqCityChangeTransactions);

    AllFlaggedTxn.addSink(StreamingFileSink
      .forRowFormat(new Path("/home/myvm/Flink/Case Study 2 - Bank Real-Time Fraud detection/Case Study 2 - Bank Real-Time Fraud detection/src/main/resources/flagged_transaction"),
        new SimpleStringEncoder < Tuple2 < String, String >> ("UTF-8"))
      .withRollingPolicy(DefaultRollingPolicy.builder().build())
      .build());
    // execute program
    env.execute("Streaming Bank");
  }

}