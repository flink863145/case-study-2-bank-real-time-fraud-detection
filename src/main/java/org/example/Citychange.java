package src.main.java.org.example;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

public  class Citychange extends ProcessWindowFunction<Tuple2< String, String >, Tuple2 < String, String > , String, TimeWindow> {
    public void process(String key, Context context, Iterable < Tuple2 < String, String >> input, Collector< Tuple2 < String, String >> out) {
        String lastCity = "";
        int changeCount = 0;
        for (Tuple2 < String, String > element: input) {
            String city = element.f1.split(",")[2].toLowerCase();

            if (lastCity.isEmpty()) {
                lastCity = city;
            } else {
                if (!city.equals(lastCity)) {
                    lastCity = city;
                    changeCount += 1;
                }
            }

            if (changeCount >= 2) {
                out.collect(new Tuple2 < String, String > ("__ALARM__", element + "marked for FREQUENT city changes"));
            }
        }
    }
}