package src.main.java.org.example;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.functions.co.KeyedBroadcastProcessFunction;
import org.apache.flink.util.Collector;
import java.util.Map;

import static src.main.java.org.example.Bank.lostCardStateDescriptor;

public class LostCardCheck extends KeyedBroadcastProcessFunction< String, Tuple2< String, String >, LostCard, Tuple2 < String, String >> {
    public void processElement(Tuple2 < String, String > value, ReadOnlyContext ctx, Collector< Tuple2 < String, String >> out) throws Exception {
        for (Map.Entry < String, LostCard> cardEntry: ctx.getBroadcastState(lostCardStateDescriptor).immutableEntries()) {
            final String lostCardId = cardEntry.getKey();
            final LostCard card = cardEntry.getValue();

            // get card_id of current transaction
            final String cId = value.f1.split(",")[5];
            if (cId.equals(lostCardId)) {
                out.collect(new Tuple2 < String, String > ("__ALARM__", "Transaction: " + value + " issued via LOST card"));
            }
        }
    }

    public void processBroadcastElement(LostCard card, Context ctx, Collector < Tuple2 < String, String >> out) throws Exception {
        ctx.getBroadcastState(lostCardStateDescriptor).put(card.id, card);
    }
}